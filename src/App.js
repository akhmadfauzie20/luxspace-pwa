import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Splash from "./pages/Splash";
import Header from "./components/Header";
import Hero from "./components/Hero";
import Browse from "./components/Browse";
import Arrived from "./components/Arrived";
import Clients from "./components/Clients";
import AsideMenu from "./components/AsideMenu";
import Footer from "./components/Footer";
import Offline from "./components/Offline";
import Profile from "./pages/Profile";

import { API_BASE_URL, ACCESS_TOKEN_NAME } from "./constants/apiContstants";


function App() {
   const [items, setItems] = React.useState([]);
   const [offlineStatus, setOfflineStatus] = React.useState(!navigator.onLine);
   const [isLoading, setIsLoading] = React.useState(true);

   function handleOfflineStatus() {
       setOfflineStatus(!navigator.onLine);
   }

   React.useEffect(function () {
       (async function () {
           const response = await fetch(API_BASE_URL, {
               headers: {
                   "Content-Type" : "application/json",
                   "Accept" : "application/json",
                   "x-api-key" : ACCESS_TOKEN_NAME,
               }
           });
           const { nodes } = await response.json();
           setItems(nodes);

           // include carousel.js
           const script = document.createElement("script");
           script.src = "/carousel.js";
           script.async = false;
           document.body.appendChild(script);
       })();

       handleOfflineStatus();
       window.addEventListener('online', handleOfflineStatus);
       window.addEventListener('offline', handleOfflineStatus);

       setTimeout(function () {
            setIsLoading(false);
       }, 1500);

       return function () {
            window.removeEventListener('online', handleOfflineStatus);
            window.removeEventListener('offline', handleOfflineStatus);
       }
   }, [offlineStatus]);

   return (
       <>
           {isLoading === true ? <Splash/> :
               (
                <>
                    {offlineStatus && <Offline/>}
                    <Header/>
                    <Hero/>
                    <Browse/>
                    <Arrived items={items} />
                    <Clients/>
                    <AsideMenu/>
                    <Footer/>
                </>
               )
           }
      </>
   );
}

export default function Routes() {
    return (
        <Router>
            <Route path="/" exact component={App}/>
            <Route path="/profile" exact component={Profile} />
        </Router>
    )
};
