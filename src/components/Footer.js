function getYear() {
    return new Date().getFullYear();
}
function Footer() {
    return (
        <footer className="flex text-center px-4 py-8 justify-center">
            <p className="text-sm">
                Copyright {getYear()}  • All Rights Reserved LuxSpace by Akhmad Fauzie
            </p>
        </footer>
    )
}

export default Footer;